#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 25 12:28:09 2018

@author: Noah Weaverdyck
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

datapath = "~/Codes/military_funding/DOD/"

myfiles = [datapath+'{0}_097_Assistance_Full_20180917.zip'.format(year) for year in (2015, 2016, 2017)]
    
mydat = pd.concat((pd.read_csv(fn) for fn in myfiles))

#mydat = pd.read_csv(datapath)
#get university or college recipients
edumask = (mydat['recipient_name'].str.contains('UNIV')) | (mydat['recipient_name'].str.contains('COLLEGE'))
unidat = mydat[edumask]

#reduce number of columns to display
maincols = ['federal_action_obligation', 'total_funding_amount', 'action_date', 'awarding_agency_name',
            'funding_agency_name','recipient_name','assistance_type_description', 'award_description']

print 'N edu entries:',len(unidat)
print 'Fraction of total:',len(unidat)*1./len(mydat)

print '\ntotal DOD funding (millions) to unis: '
print unidat['federal_action_obligation'].sum()*1.e-6

print unidat['recipient_name'].value_counts()
print '\n2017 Award sum (millions)'
print unidat.groupby(['recipient_name'])['federal_action_obligation'].agg('sum').sort_values(ascending=False)*(1.e-6)

print '\nTop awards info: '
print unidat.sort_values(by='federal_action_obligation', ascending=False).head()[maincols]

#full descruption of highest award amounts at UMichigan
michdat = unidat[unidat['recipient_name'].str.contains('OF MICHIGAN')].sort_values(by='federal_action_obligation', ascending=False)
print '\nTop UMich Awards'
for i in xrange(20): #need to use iloc[i] to ref string directly and print the full string
    print '${0:.3f} Million, {1}'.format(michdat['federal_action_obligation'].iloc[i]*1e-6, michdat['award_description'].iloc[i])
